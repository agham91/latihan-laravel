<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>registrasi</title>
</head>
<body>
    <h1>Buat Account Baru</h1><br>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label for="name">First name</label><br>
        <input type="text" name="full_name" id="name"><br><br>

        <label for="lname">Last Name</label><br>
        <input type="text" name="lname" id="lname"><br><br>

        <label for="gender">Gender</label><br>
        <input type="radio" name="gender" id="gender">Male <br>
         <input type="radio" name="gender" id="gender">Female <br>
         <input type="radio" name="gender" id="gender">Other <br>

         <label for="negara">Nationality :</label><br><br>
         <select name="negara" id="negara">
             <option value="indonesia">Indonesia</option>
             <option value="malaysia">Malaysia</option>
             <option value="arab">Arab Saudi</option>
             <option value="inggris">Inggris</option>
         </select><br><br>

         <label>Language Spoken :</label><br><br>
         <input type="checkbox">Bahasa Indonesia<br>
         <input type="checkbox">English<br>
         <input type="checkbox">Arab<br>
         <input type="checkbox">Other<br><br>

         <label for="bio">Bio</label><br>
         <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br><br>

        <input type="submit" value="SignUp">

    
    </form>

</body>
</html>